# tile, tilemap
import sys
import pygame, os
from pygame.locals import *
import xml.etree.ElementTree as ET
from settings import BASE_DIR

class Tile(pygame.sprite.Sprite):
    """
    Tile class for all the stuff you put in the Tile Layer in Tiled.
    Used for stationary stuff like standard platforms, goal, key, stationary death.

    The type attribute is very important, used to assign tombstone, key, death and
    add the standard platforms to the collision system.
    """
    def __init__(self, tilemap, tileid, x, y, tile_type):
        pygame.sprite.Sprite.__init__(self)
        self.parent = tilemap
        self.tileid = tileid
        self.width = self.parent.tilew
        self.height = self.parent.tileh
        self.type = tile_type
        
        self.rect = pygame.Rect(x, y, self.width, self.height)

        tileset_rect = pygame.Rect((tileid%self.width)*self.width, (tileid/self.width)*self.height, self.width, self.height)
        self.image = self.parent.tileset.subsurface(tileset_rect)
        
    def __str__(self):
        return "Position in map -- ({0}, {1})".format(self.pos[0], self.pos[1])

class Tilemap(pygame.sprite.LayeredUpdates):
    """
    Parses the .tmx file which comes from Tiled.
    
    """
    def __init__(self, tileset, data):
        pygame.sprite.LayeredUpdates.__init__(self)
        self.tileset_path = tileset
        self.data_path = data
        self.tileid_to_type = {}           # is the mapping from tile numbers to their types
        self.initialize_mapdata()

    def initialize_mapdata(self):
        """
        Sets global values for tilemap and other variables to make Tile objects.
        """
        self.tileset = pygame.image.load(self.tileset_path).convert()
        self.data = ET.parse(self.data_path)
        root = self.data.getroot()
        
        self.map_width = int(root.get('width'))
        self.map_height = int(root.get('height'))
        self.tilew = int(root.get('tilewidth'))
        self.tileh = int(root.get('tileheight'))

        # constructing the tileid_to_type dictionary from the tileset xml
        for elem in self.data.find('tileset').findall('tile'):
            for pty in elem.find('properties').findall('property'):
                if pty.get('name') == 'type':
                    self.tileid_to_type[elem.get('id')] = pty.get('value')

        self.create_tiles()
        
    def create_tiles(self):
        """
        Adds the tiles to the builtin spritelist of pygame.LayeredUpdates
        at layer 0. There is no guarantee of order of drawing in a given layer.
        """
        self.tiles = []
        tiledata = self.data.find('layer').find('data').text.split(',')
        
        for index, elem in enumerate(tiledata):
            row = index/self.map_width
            column = index%self.map_width
            if elem[0] == '\n':
                tileid = int(elem[1:]) - 1
            elif elem[-1] == '\n':
                tileid = int(elem[:-1]) - 1
            else:
                tileid = int(elem) - 1
            tile_type = self.tileid_to_type[str(tileid)]
            self.tiles.append(Tile(self, tileid, column*self.tilew, row*self.tileh, tile_type))
        
        # add for drawing
        self.add(self.tiles, layer=0)

    def get_object(self, name, otype):
        """
        Retrives object xml of given name and type from the .tmx file's object layer.
        """
        for obj in self.data.find('objectgroup').findall('object'):
            if obj.get('name') == name and obj.get('type') == otype:
                return obj
                break

if __name__ == '__main__':
    pygame.init()
    size = (800, 800)
    screen = pygame.display.set_mode(size)
    tm = Tilemap(os.path.join(BASE_DIR, '../assets/images/generic_platformer_tiles.png'),
                 os.path.join(BASE_DIR, '../assets/tilemaps/betterlevel3.tmx'))
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        tm.draw(screen)
        pygame.display.flip()
