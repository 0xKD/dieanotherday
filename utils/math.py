class Vec2:
	def __init__(self, x, y):
		self.x = x
		self.y = y

	def __add__(self, other):
		return Vec2(self.x + other.x, self.y + other.y)

	def __mul__(self, other):
		if isinstance(other, int) or isinstance(other, float):
			return Vec2(self.x*other, self.y*other)
		elif isinstance(other, Vec2):
			# do a dot product
			return Vec2(self.x*other.x, self.y*other.y)

	def __str__(self):
		return "({0}, {1})".format(self.x, self.y)

	def coords(self):
		return (self.x, self.y)