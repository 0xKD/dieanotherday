import pygame
from pygame.locals import *
from settings import TILE_WIDTH, TILE_HEIGHT 

class Entity(pygame.sprite.Sprite):
    """
    This is the class for animated objects other than Player.
    Moving, linked platforms, oscillating death come under this for now.
    Takes xml from .tmx file and creates a drawable sprite.
    Subclasses will use the properties attribute and define their own
    update method.
    """
    def __init__(self, xml_obj, tilemap):
        pygame.sprite.Sprite.__init__(self)
        tileset = tilemap.tileset

        self.rect = pygame.Rect(int(xml_obj.get('x')),
                                int(xml_obj.get('y')),
                                int(xml_obj.get('width')),
                                int(xml_obj.get('height')))

        # Get the tile from the tileset, repeat the same tile (object width/tile width) times.
        for tileid, tile_type in tilemap.tileid_to_type.iteritems():
            if tile_type == xml_obj.get('type'):
                tileset_rect = pygame.Rect((int(tileid)%TILE_WIDTH)*TILE_WIDTH,
                                           (int(tileid)/TILE_WIDTH)*TILE_HEIGHT,
                                           TILE_WIDTH,
                                           TILE_HEIGHT)
                tile_image = tileset.subsurface(tileset_rect)
                self.image = pygame.Surface((int(xml_obj.get('width')), int(xml_obj.get('height'))))

                for x in range(int(xml_obj.get('width'))/TILE_WIDTH):
                    target_rect = pygame.Rect(x*TILE_WIDTH, 0, TILE_WIDTH, TILE_HEIGHT)
                    self.image.blit(tile_image, target_rect)
                break

        # all the properties specified in the editor
        self.properties = {}
        for prop in xml_obj.find("properties").findall("property"):
            self.properties[prop.get("name")] = prop.get("value")
