import pygame
import os
from pygame.locals import *
from utils.math import Vec2
from settings import BASE_DIR, gravity

class Player(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        # anim-related
        self.spritesheet = pygame.image.load(os.path.join(BASE_DIR, 'assets/images/dude.png')).convert()
        self.spritesheet.set_colorkey((255, 255, 255, 1))
        self.tile_width = 32
        self.tile_height = 48
        self.idle = 4
        self.left_start = 0
        self.left_end = 3
        self.right_start = 5
        self.right_end = 8
        self.anim_state = "idle"
        self.curr_frame = self.idle
        # anim-delay
        pygame.time.set_timer(pygame.USEREVENT + 1, 95)
        self.image = self.spritesheet.subsurface((self.tile_width * self.curr_frame, 0, self.tile_width, self.tile_height))

        # physics-related
        self.rect = self.image.get_rect()
        self.move_speed = 100
        self.jump_speed = -250
        self.speed = Vec2(0, 0)
        
        # state handling
        self.moving_right = False
        self.moving_left = False
        self.falling_down = True
        self.jumping = False
        self.dead = False
        self.on_platform = False
        self.on_moving_platform = False

    def set_startpos(self, posxy):
        """
        Used to set the starting position of the player in the level.
        """
        self.pos = Vec2(posxy[0], posxy[1])
        self.rect.topleft = self.pos.coords()

    def land_on_platform(self, platform):
        """
        stuff that should happen when you land on any (linked, moving, not moving) platform
        """
        self.speed.y = 0
        self.on_platform = True
        if self.falling_down:
            self.pos.y = platform.top - self.tile_height + 1
            self.falling_down = False
            if self.dead:
                self.dead = False

    def land_on_moving_platform(self, mplat):
        self.land_on_platform(mplat.rect)
        # only one of xspeed and yspeed can be nonzero
        self.moving_plat_speed = mplat.speed
        if not self.on_moving_platform:
            self.speed.x = self.moving_plat_speed.x
            self.on_moving_platform = True
        
    def handle_death(self, tombstone):
        if not self.dead:
            self.dead = True
            self.pos = Vec2(tombstone.rect.left, tombstone.rect.top - 20)
            self.speed.y = -self.speed.y
        else:
            self.pos = Vec2(0, 0)
            self.dead = False
            self.speed = Vec2(0, 0)
    
    def debug_data(self):
        print 'Jumping -- ', self.jumping
        print 'Falling -- ', self.falling_down
        print 'Death -- ', self.dead
        print 'Facing Right -- ', self.moving_right
        print 'On a platform -- ', self.on_platform
        print 'On a moving platform -- ', self.on_moving_platform
        print '-'*20

    def update(self, timestep):
        print self.speed
        self.debug_data()
        
        # if moving right
        if self.moving_right:
            if self.on_moving_platform:
                self.speed.x = self.move_speed + self.moving_plat_speed.x
            else:
                self.speed.x = self.move_speed
        # if moving left
        elif self.moving_left:
            if self.on_moving_platform:
                self.speed.x = -self.move_speed + self.moving_plat_speed.x
            else:
                self.speed.x = -self.move_speed
        # if standing still
        else:
            if self.on_moving_platform:
                self.speed.x = self.moving_plat_speed.x
            else:
                self.speed.x = 0

        # more state handling
        if not self.on_platform and not self.jumping:
            self.falling_down = True
        if not self.on_moving_platform:
            self.on_platform = False # hack: so that player doesnt stand on thin air

        if self.on_moving_platform:
            self.speed.y = self.moving_plat_speed.y
        # this is the physics code
        # euler integration with small timestep (i think)
        if self.falling_down or self.jumping:
            self.speed.y += gravity * timestep
        self.pos += self.speed * timestep
        # it ends here

        # handling states obsessively
        if self.jumping and self.speed.y > 0:
            self.jumping = False
            self.falling_down = True
        self.rect.topleft = self.pos.coords()

    def handle_animations(self):
        if self.anim_state == "idle":
            self.curr_frame = self.idle
        elif self.anim_state == "walk_right":
            if self.curr_frame not in range(self.right_start, self.right_end + 1):
                self.curr_frame = self.right_start
            else:
                if self.curr_frame == self.right_end:
                    self.curr_frame = self.right_start
                else:
                    self.curr_frame += 1
        elif self.anim_state == "walk_left":
            if self.curr_frame not in range(self.left_start, self.left_end + 1):
                self.curr_frame = self.left_start
            else:
                if self.curr_frame == self.left_end:
                    self.curr_frame = self.left_start
                else:
                    self.curr_frame += 1
        # could be optimized by saving all anim frames in a list and just iterating instead of
        # doing the subsurface operation every update.
        self.image = self.spritesheet.subsurface((self.tile_width * self.curr_frame, 0, self.tile_width, self.tile_height))

    def handle_events(self, event):
        if not self.dead:
            """
            Enables walking and jumping while the player is not dead.
            """
            if event.type == KEYDOWN:
                if event.key == K_RIGHT:
                    self.anim_state = 'walk_right'
                    self.moving_right = True
                    self.moving_left = False
                elif event.key == K_LEFT:
                    self.anim_state = 'walk_left'
                    self.moving_right = False
                    self.moving_left = True
            elif event.type == KEYUP:
                if event.key == K_RIGHT or event.key == K_LEFT:
                    self.anim_state = 'idle'
                    self.moving_right = False
                    self.moving_left = False

            #jumping
            if not self.falling_down and not self.jumping:
                if event.type == KEYDOWN and event.key == K_UP:
                    self.anim_state = 'jumping'
                    # hack so that player doesnt get stuck in tile
                    self.rect.top -= 1
                    # end hack
                    self.speed.y = self.jump_speed
                    self.jumping = True
                    self.on_platform = False
                    self.on_moving_platform = False
                    
        if event.type == pygame.USEREVENT + 1:
            self.handle_animations()
