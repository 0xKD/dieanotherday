import pygame
from utils.math import Vec2
# from utils.tiles import Tile

class Tombstone(pygame.Rect):
    def __init__(self, descr, hitbox):
        self.descr = descr
        self.hitbox = hitbox
        self.pos = Vec2(self.hitbox.topleft[0], self.hitbox.topleft[1])

class Death(pygame.Rect):
    def __init__(self, descr, hitbox):
        self.descr = descr
        self.hitbox = hitbox
        self.pos = Vec2(self.hitbox.topleft[0], self.hitbox.topleft[1])