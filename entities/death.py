import pygame
from pygame.locals import *
from settings import TILE_WIDTH, TILE_HEIGHT 
from base_entity import Entity

class OscillatingDeath(Entity):
    def __init__(self, xml_obj, tilemap):
        Entity.__init__(self, xml_obj, tilemap)
        self.upper_limit = self.rect.top - int(self.properties['upper_limit'])*TILE_WIDTH
        self.lower_limit = self.rect.top + int(self.properties['lower_limit'])*TILE_HEIGHT
        self.flag = True
        self.speed = 3
        
    def update(self, ticks):
        """
        Upper and lower are wrt the player so upper y coordinate is actually lesser than the
        lower y coordinate.

        Moves up it till it reaches upper limit then moves down till it reaches lower limit, repeat.
        """
        if self.flag:
            self.rect.move_ip(0, -self.speed)
            if self.rect.top < self.upper_limit:
                self.flag = False
        else:
            self.rect.move_ip(0, self.speed)
            if self.rect.bottom > self.lower_limit:
                self.flag = True
            
