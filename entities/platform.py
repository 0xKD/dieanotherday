from base_entity import Entity
from utils.math import Vec2

class MovingPlatform(Entity):
    def __init__(self, xml_obj, tilemap):
        Entity.__init__(self, xml_obj, tilemap)

        self.xspeed = int(self.properties['xspeed'])
        self.yspeed = int(self.properties['yspeed'])
        self.speed = Vec2(self.xspeed, self.yspeed)
        self.pos = Vec2(self.rect.topleft[0], self.rect.topleft[1])
        
    def update(self, ticks):
        self.pos += self.speed*ticks
        self.rect.topleft = self.pos.coords()
