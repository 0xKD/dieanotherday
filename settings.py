import os

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
gravity = 500.0
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 640
TILE_WIDTH = 32
TILE_HEIGHT = 32
