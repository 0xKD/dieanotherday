import os, sys, pygame
from pygame.locals import *
from settings import BASE_DIR, SCREEN_WIDTH, SCREEN_HEIGHT
from base_level import Level
from entities.player import Player
from entitites.platform import MovingPlatform

class Level7(Level):
    def __init__(self, screen, tileset_path, tilemap_path, player):
        Level.__init__(self, screen, tileset_path, tilemap_path, player)
        
if __name__ == '__main__':
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    dude = Player()
    tileset_path = os.path.join(BASE_DIR, "assets/images/generic_platformer_tiles.png")
    tilemap_path = os.path.join(BASE_DIR, "assets/tilemaps/testing4.tmx")
    level = Level7(screen, tileset_path, tilemap_path, dude)
    level.run()
