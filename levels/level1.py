import sys, os, pygame
from pygame.locals import *
from settings import BASE_DIR
from utils.tiles import Tilemap
from entities.player import Player
from base_level import Level

class Level1(Level):
	def __init__(self, screen, tileset_path, data_path, player):
		Level.__init__(self, screen, tileset_path, data_path, player)

		# level specific code
		self.player.set_startpos(250, 400)
		self.run()

if __name__ == '__main__':
	pygame.init()
	screen = pygame.display.set_mode((800, 800))
	dude = Player()
	level = Level1(screen, os.path.join(BASE_DIR, 'assets/images/generic_platformer_tiles.png'),
				   os.path.join(BASE_DIR, 'assets/tilemaps/revisedlevel1.tmx'), dude)