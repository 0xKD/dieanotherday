import os, sys, pygame
from pygame.locals import *
from settings import BASE_DIR
from utils.tiles import Tilemap
from entities.player import Player

class Level(Tilemap):
    """
    Base class for all levels.

    The main game loop lives in the run method.
    All game objects are initialized in the initialize_entities method. Usually overridden.
    The handle_collision method is the collision system for the game. Usually overridden.

    The level_complete method is hook for clean exit and transition to the next level.
    """
    def __init__(self, screen, tileset_path, tilemap_path, player):
        Tilemap.__init__(self, tileset_path, tilemap_path)
        self.screen = screen
        self.player = player
        self.timer = pygame.time.Clock()
        
        self.standard_plats = []
        self.deadly = []
        self.tombstone = None
        self.key = None
        self.goal = None
        self.check_goal = False

        self.level_running = True

    def run(self):
        while True:
            if self.level_running:
                ticks = self.timer.tick(60)/1000.0

                # handle events, delegate everything other than EXIT to player
                for event in pygame.event.get():
                    if event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                        self.level_complete(0)
                    else:
                        self.player.handle_events(event)
                # the collision system
                self.handle_collision()

                # update the level and the player
                self.update(ticks)
                self.player.update(ticks)

                # clear the screen and redraw
                self.screen.fill((0, 0, 0, 1))
                self.draw(self.screen)
                self.screen.blit(self.player.image, self.player.rect)

                pygame.display.flip()
            else:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                        os._exit(1)

                # just display the level complete message
                self.screen.fill((0, 0, 0, 1))
                self.draw(self.screen)
                self.screen.blit(self.player.image, self.player.rect)
                self.draw_level_complete_text()

                pygame.display.flip()
                
                    

    def initialize_entities(self, tile):
        """
        Hook to call in from subclass. Classifies stationary tiles based on type
        and caches for use in the collision system, death-respawn and key-goal logic.
        """
        tile_type = tile.type
        if tile_type == "background":
            pass
        elif tile_type == "standard_platform":
            self.standard_plats.append(tile.rect)
        elif tile_type == "tombstone":
            self.tombstone = tile
        elif tile_type == "death":
            self.deadly.append(tile.rect)
        elif tile_type == "key":
            self.key = tile
        elif tile_type == "goal":
            self.goal = tile
        
    def handle_collision(self):
        """
        THE LEVEL handles the collisions. 
        Calls methods on player for changing the player's state/speed, etc.

        Should be overridden. Provides standard platform, standard death, key
        and goal logic.
        """
        for collider in self.standard_plats:
            if self.player.rect.colliderect(collider):
                self.player.land_on_platform(collider)

        for death in self.deadly:
            if self.player.rect.colliderect(death):
                if self.tombstone:
                    self.player.handle_death(self.tombstone)
                else:
                    print "ERROR: You didnt assign a tombstone!"
                break

        if self.key:
            if pygame.sprite.collide_rect(self.player, self.key):
                self.check_goal = True
        else:
            print "ERROR: You didnt assign a key!"

        if self.goal:
            if self.check_goal is not None:
                if pygame.sprite.collide_rect(self.player, self.goal):
                    self.level_complete(1)
        else:
            print "ERROR: You didnt assign a goal!"
            
    def level_complete(self, flag):
        if flag == 1:
            self.level_running = False
        else:
            print "You didnt clear the level, but I'm letting you go."
            sys.exit()

    def draw_level_complete_text(self):
        pass

if __name__ == '__main__':
    pygame.init()
    screen = pygame.display.set_mode((800, 800))
    dude = Player()
    dude.set_startpos(50, 500)
    level = Level(screen,
                  os.path.join(BASE_DIR, "assets/images/generic_platformer_tiles.png"),
                  os.path.join(BASE_DIR, "assets/tilemaps/betterlevel3.tmx"),
                  dude)
    level.run()
