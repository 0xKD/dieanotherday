import os, sys, pygame
from pygame.locals import *
from base_level import Level
from entities.player import Player
from entities.platform import MovingPlatform
from settings import BASE_DIR, SCREEN_WIDTH, SCREEN_HEIGHT

class Level4(Level):
    def __init__(self, screen, tileset_path, tilemap_path, player):
        Level.__init__(self, screen, tileset_path, tilemap_path, player)

        self.initialize_entities()

    def initialize_entities(self):
        for t in self.sprites():
            Level.initialize_entities(self, t)
        pos = self.get_object("player_startpos", "position")
        self.player.set_startpos((int(pos.get('x')), int(pos.get('y'))))

        m = MovingPlatform(self.get_object("random", "moving_platform"), self)
        self.add(m, layer=1)
        self.moving_plats = [m]
        
    def handle_collision(self):
        Level.handle_collision(self)

        # moving platform collision code
        for coll in self.moving_plats:
            if pygame.sprite.collide_rect(coll, self.player):
                self.player.land_on_moving_platform(coll)                
        
if __name__ != '__main__':
    pygame.init()
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    dude = Player()
    tileset_path = os.path.join(BASE_DIR, "assets/images/generic_platformer_tiles.png")
    tilemap_path = os.path.join(BASE_DIR, "assets/tilemaps/testing.tmx")
    level = Level4(screen, tileset_path, tilemap_path, dude)
    level.run()
