import os, sys, pygame
from pygame.locals import *
from settings import BASE_DIR, SCREEN_HEIGHT, SCREEN_WIDTH, TILE_WIDTH, TILE_HEIGHT 
from entities.player import Player
from entities.tombstone import Tombstone
from entities.platform import ConnectedPlatform
from entities.death import OscillatingDeath
from levels.base_level import Level
from utils.tiles import Tilemap

"""
Design idea -- each time you respawn from the tombstone, it moves, your respawn is the trigger.
"""
class Level3(Level):
	def __init__(self, screen, tileset_path, tilemap_path, player):
		Level.__init__(self, screen, tileset_path, tilemap_path, player)

		# level specific code
		self.valley_of_lava = OscillatingDeath(4*TILE_WIDTH, SCREEN_HEIGHT - 7*TILE_HEIGHT, 17*TILE_WIDTH, 7*TILE_HEIGHT)
		self.player.attach_death([self.valley_of_lava])
		
		conn_width = 4*TILE_WIDTH
		conn_height = TILE_HEIGHT
		first = ConnectedPlatform(10*TILE_WIDTH, 11*TILE_HEIGHT, conn_width, conn_height)
		second = ConnectedPlatform(10*TILE_WIDTH, 19*TILE_HEIGHT, conn_width, conn_height)
		third = ConnectedPlatform(10*TILE_WIDTH, 3*TILE_HEIGHT, conn_width, conn_height)
		fourth = ConnectedPlatform(16*TILE_WIDTH, 3*TILE_HEIGHT, TILE_WIDTH, conn_height)
		second.add_next(first)
		first.add_next(third)
		third.add_next(fourth)
		self.linked_plats = pygame.sprite.Group()
		self.linked_plats.add(second)
		self.player.attach_linked_platforms(self.linked_plats)
		self.run()

	def run(self):
		while True:
			for event in pygame.event.get():
				if event.type == pygame.QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
				    self.level_complete()
				else:
				    self.player.handle_events(event)
			self.valley_of_lava.update()
			self.player.update(self.timer.tick(60)/1000.0)
			self.draw(self.screen)
			self.screen.blit(self.player.image, self.player.rect)
			self.screen.blit(self.valley_of_lava.image, self.valley_of_lava.rect)
			self.linked_plats.draw(self.screen)
			pygame.display.flip()

if __name__ == '__main__':
	pygame.init()
	screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
	dude = Player()
	dude.set_startpos(2*TILE_WIDTH, 11*TILE_HEIGHT)
	tileset_path = os.path.join(BASE_DIR, "assets/images/generic_platformer_tiles.png")
	tilemap_path = os.path.join(BASE_DIR, "assets/tilemaps/mechanic_testing.tmx")
	level = Level3(screen, tileset_path, tilemap_path, dude)
